// Activity codes

// #3 

fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => console.log(json));

// #4

fetch("https://jsonplaceholder.typicode.com/todos")
  .then((response) => response.json())
  .then((data) => console.log(data.map(todo => todo.title)));

// #5

fetch("https://jsonplaceholder.typicode.com/todos/1")
  .then(response => response.json())
  .then(data => console.log(data))
  .catch(error => console.log(error));

// #6

fetch("https://jsonplaceholder.typicode.com/todos/1")
  .then(response => response.json())
  .then(data => {
    console.log({
        "Title" : data.title, 
        "Status" : data.completed
    });
  }).catch(error => console.log(error));

// #7

fetch("https://jsonplaceholder.typicode.com/todos", {
  method: "POST",
  body: JSON.stringify({ title: "New To-Do Item", completed: false }),
  headers: {
    "Content-type": "application/json; charset=UTF-8"
  }
})
  .then(response => response.json())
  .then(data => console.log(data))
  .catch(error => console.log(error));

// #8

fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "PUT",
  body: JSON.stringify({ title: "Updated To-Do Item", completed: true }),
  headers: {
    "Content-type": "application/json; charset=UTF-8"
  }
})
  .then(response => response.json())
  .then(data => console.log(data))
  .catch(error => console.log(error));

// #9

fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "PUT",
  body: JSON.stringify({
    title: "New Title",
    description: "New Description",
    status: "Completed",
    dateCompleted: new Date(),
    userId: 1
  }),
  headers: {
    "Content-type": "application/json; charset=UTF-8"
  }
})
  .then(response => response.json())
  .then(data => console.log(data))
  .catch(error => console.log(error));

// #10

fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "PATCH",
  body: JSON.stringify({
    title: "Updated Title"
  }),
  headers: {
    "Content-type": "application/json"
  }
})
  .then(response => response.json())
  .then(data => console.log(data))
  .catch(error => console.log(error));

// #11

fetch("https://jsonplaceholder.typicode.com/todos/2", {
  method: "PATCH",
  body: JSON.stringify({
    status: "Completed",
    dateCompleted: new Date()
  }),
  headers: {
    "Content-type": "application/json"
  }
})
  .then(response => response.json())
  .then(data => console.log(data))
  .catch(error => console.log(error));

// #12

fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "DELETE"
})
  .then(response => response.json())
  .then(data => console.log(data))
  .catch(error => console.log(error));

  








