// [SECTION] Javascript Synchronous vs Asynchronous
// Javascript, by default is synchronous - this means that only one statement is executed at a time

console.log("Hello");

// Asynchronous means that we can proceed to execute other statements, while consuming code running in the background.

// The Fetch API
/*

- Allows you to asynchronously request for a resource (data)
- A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value

It returns:
- result or data
- Pending
- Error

Syntax:

    fetch("URL")

*/

// Promise Pending

console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

// Syntax

// fetch("URL")
// .then((response) => {})

// Retrieving all posts following the REST API (retrieve, /posts, GET)
// By using the then method we can now check for the status of the promise

// The 'fetch' method will return a "promise" that resolves to a "Response" Object
// The 'then' method captures the "response" object and returns another 'promise' which will eventually be "resolve" or "rejected"

fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => console.log(response.status));

// =========================== With JSON RESPONSE ==============================

fetch("https://jsonplaceholder.typicode.com/posts")

// Use the "json" method from the "response" object to convert data retrieved into JSON format to be used in our application

.then((response) => response.json())

// Print the converted JSON value from the "fetch" request
// Using multiple "then" methods creates a "promise chain"

.then((json) => console.log(json));

// [SECTION] Getting a specific post allowing the REST API (retrieve, /posts/id, GET)

fetch("https://jsonplaceholder.typicode.com/posts")
.then((response) => response.json())
.then((json) => console.log(json));

// [SECTION] Creating a post

// fetch("URL", options)
// .then((response) => {})
// .then((response) => {});

// Create a new post following the REST API (create, /posts, POST)

fetch("https://jsonplaceholder.typicode.com/posts", {

    // Sets the method of the "Request" object to "POST" following REST API
	// Default method is GET
    method: "POST",

    // Sets the header data of the "Request" object to be sent to the backend
	// Specified that the content will be in a JSON structure
    headers: {
        "Content-Type": "application/json",
    },

    // Sets the content/body data of the "Request" object to be sent to the backend
	// JSON.stringify converts the object data into a stringified JSON
    body: JSON.stringify({
        title: "New Post",
        body: "Hello World",
        userId: 101
    })
}).then((response) => response.json())
.then((json) => console.log(json));

// [Section] Updating a post using PUT method

// Updates a specific post following the Rest API (update, /posts/:id, PUT)
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
  	'Content-type': 'application/json',
	},
	body: JSON.stringify({
	  	id: 1,
	  	title: 'Updated post',
	  	body: 'Hello again!',
	  	userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// [Section] Updating a post using PATCH method

// Updates a specific post following the Rest API (update, /posts/:id, Patch)
// The difference between PUT and PATCH is the number of properties being changed
// PATCH is used to update the whole object
// PUT is used to update a single/several properties
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
  	'Content-type': 'application/json',
	},
	body: JSON.stringify({
	  	title: 'Corrected post',
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// [Section] Deleting a post

// Deleting a specific post following the Rest API (delete, /posts/:id, DELETE)
fetch('https://jsonplaceholder.typicode.com/posts/1', {
  method: 'DELETE'
});

// ==============================================================================

/*

The fetch() method in JavaScript is used to retrieve resources from a server. It is part of the Fetch API, which allows you to make network requests similar to XMLHttpRequest (XHR) or the more modern and powerful fetch(). The fetch() method returns a promise that resolves to the Response object representing the response to the request. The Response object can be read using various methods such as text(), json(), formData(), etc. Here is an example of how you might use fetch() to make a GET request to an API endpoint and retrieve the JSON data:

*/

fetch('https://jsonplaceholder.typicode.com/posts')
  .then(response => response.json())
  .then(data => {
    console.log(data);
  })
  .catch(error => {
    console.error('Error:', error);
  });

// You can also include option for example method, headers in the fetch call by passing it as an options object:

fetch('https://jsonplaceholder.typicode.com/posts', {
  method: 'POST', // *GET, POST, PUT, DELETE, etc.
  mode: 'cors', // no-cors, *cors, same-origin
  cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
  credentials: 'same-origin', // include, *same-origin, omit
  headers: {
    'Content-Type': 'application/json'
    // 'Content-Type': 'application/x-www-form-urlencoded',
  },
  redirect: 'follow', // manual, *follow, error
  referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
  body: JSON.stringify({
    title: "New Post",
    body: "Hello World!",
    userId: 102
}) // body data type must match "Content-Type" header
}).then((response) => response.json())
.then((json) => console.log(json));

// It is important to note that the fetch() method is an asynchronous operation, which means that the rest of your JavaScript code will continue to execute while the request is being made. To handle the data returned by the API, you need to use the then() method to attach callbacks to the promise returned by fetch().










